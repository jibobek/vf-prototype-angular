import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { PageUrl } from 'src/app/enum/PageUrl';
import { ContactService } from 'src/app/services/contact.service';
import { RouterService } from 'src/app/services/router.service';
import { Contact } from 'src/app/types/Contact';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.page.html',
  styleUrls: ['./directory.page.scss'],
})
export class DirectoryPage implements OnInit {

  public contacts: Array<Contact> = [];
  public isLoading: boolean = false;
  public contactDisplayNames: Array<string> = [];

  constructor(
    private contactService: ContactService,
    private routerService: RouterService,
    private platform: Platform,
  ) { 
    this.platform.backButton.subscribeWithPriority(10, () => this.routerService.goToPage(PageUrl.HOME));
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.isLoading = true;
    this.contactService.getContactList().then(contacts => {
      this.contacts = contacts
      this.isLoading = false;
      this.updateContactDisplayNames();
    });
  }

  public loadNext(event) {
    this.contactService.getContactListNext().then(contacts => {
      this.contacts = this.contacts.concat(contacts);
      this.updateContactDisplayNames();
      event.target.complete();
    });
  }

  public openContact(contactId: number): Promise<boolean> {
    return this.routerService.goToPageWithId(PageUrl.CONTACT, contactId);
  }

  public createContact(): void {
    this.routerService.goToPageWithId(PageUrl.CONTACT, 0);
  }

  private updateContactDisplayNames(): void {
    this.contactDisplayNames = [];
    for (let i = 0; i < this.contacts.length; i++) {
      const c = this.contacts[i];
      this.contactDisplayNames.push(c.name);
    }
  }
}
