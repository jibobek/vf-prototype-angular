import { Component, OnInit } from '@angular/core';
import { Invoice } from 'src/app/types/Invoice';
import { ActivatedRoute } from '@angular/router';
import { InvoiceService } from 'src/app/services/invoice.service';
import { AlertService } from 'src/app/services/alert.service';
import { RouterService } from 'src/app/services/router.service';
import { PageUrl } from 'src/app/enum/PageUrl';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.page.html',
  styleUrls: ['./invoice.page.scss'],
})
export class InvoicePage implements OnInit {

  public invoice: Invoice;
  public invoiceId: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    public invoiceService: InvoiceService,
    private alertService: AlertService,
    private routerService: RouterService,
    private platform: Platform,
  ) { 
    this.platform.backButton.subscribeWithPriority(10, () => this.routerService.goToPage(PageUrl.INVOICES));
  }

  ngOnInit() {
    this.invoiceId = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewDidEnter() {
    this.loadInvoice();
  }

  private loadInvoice(): Promise<Invoice | void> {
    return this.invoiceService.getInvoice(this.invoiceId).then(invoice => this.invoice = invoice);
  }

  public openInvoiceOptions(): void {
    let options = [
      {
        text: 'Upravit',
        role: '',
        handler: () => { this.routerService.goToPageWithId(PageUrl.INVOICE_CREATE, this.invoice.invoiceId) }
      }];
    options.push(
      {
        text: 'Smazat',
        role: '',
        handler: () => this.invoiceService.delete(this.invoice).then(() => this.routerService.goToPage(PageUrl.INVOICES))
      }
    );
    options.push(
      {
        text: ((this.invoiceService.translateInvoiceFlags(this.invoice.flags).storno) ? 'Odstornovat' : 'Stornovat'),
        role: '',
        handler: () => this.invoiceService.stornoInvoice(this.invoice, this.invoiceService.translateInvoiceFlags(this.invoice.flags).storno === false).then(() => this.loadInvoice())
      }
    );
    if (this.invoiceService.translateInvoiceFlags(this.invoice.flags).paid === false) {
      options.push({
        text: 'Uhradit',
        role: '',
        handler: () => this.invoiceService.payInvoice(this.invoice)
      });
      options.push({
        text: (this.invoiceService.translateInvoiceFlags(this.invoice.flags).archived) ? 'Odarchivovat' : 'Archivovat',
        role: '',
        handler: () => this.invoiceService.archiveInvoice(this.invoice, this.invoiceService.translateInvoiceFlags(this.invoice.flags).archived === false).then(() => this.loadInvoice())
      });
    }
    if (this.invoice.eetStatus === 4) {
      options.push({
        text: 'Zaslat do EET',
        role: '',
        handler: () => this.invoiceService.sendToEet(this.invoice).then(() => this.loadInvoice())
      });
    }
    options.push({
      text: 'Veřejná stránka dokladu',
      role: '',
      handler: () => this.invoiceService.openPublicInvoicePage(this.invoice)
    });
    options.push({
      text: 'Zavřít',
      role: 'cancel',
      handler: () => { }
    });
    this.alertService.show("Možnosti", "Doklad " + this.invoice.number, options);
  }
}
