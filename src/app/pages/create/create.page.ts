import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { PageUrl } from 'src/app/enum/PageUrl';
import { ContactService } from 'src/app/services/contact.service';
import { DateService } from 'src/app/services/date.service';
import { InvoiceService } from 'src/app/services/invoice.service';
import { RouterService } from 'src/app/services/router.service';
import { StorageService } from 'src/app/services/storage.service';
import { InvoiceForm, InvoiceFormItem } from 'src/app/types/InvoiceForm';
import { ActivatedRoute } from '@angular/router';
import { Contact } from 'src/app/types/Contact';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  private readonly DEFAULT_CURRENCY = 'CZK';
  private readonly DEFAULT_UNIT = 'ks';
  private readonly DEFAULT_VAT = '21';
  private readonly DEFAULT_DUE_DAYS = '10';

  public formData: InvoiceForm = {
    customerId: '',
    number: '',
    paymentMethodId: '',
    numberSeriesId: '',
    daysDue: '',
    dateCreated: '',
    currency: '',
    vs: '',
    ks: '',
    ss: '',
    items: [],
    customerIco: '',
    customerDic: '',
    customerName: '',
    customerStreet: '',
    customerCity: '',
    customerZip: '',
    customerCountryCode: '',
  };

  public paymentMethodsForSelect: Array<{ id: string, name: string }> = [];
  public numberSeriesForSelect: Array<{ id: string, name: string }> = [];
  public customersForSelect: Array<{ id: string, name: string }> = [];
  public invoiceId: number;

  constructor(
    private toastController: ToastController,
    private invoiceService: InvoiceService,
    private routerService: RouterService,
    public storageService: StorageService,
    private contactService: ContactService,
    private dateService: DateService,
    private activatedRoute: ActivatedRoute,
    private changeRef: ChangeDetectorRef,
    private platform: Platform,
  ) { 
    this.platform.backButton.subscribeWithPriority(10, () => this.routerService.goToPage(PageUrl.HOME));
  }

  ngOnInit() {
    this.invoiceId = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewDidEnter() {
    if (this.invoiceId === 0) {
      this.initForm();
    } else {
      this.loadInvoice(this.invoiceId);
    }
    this.prepareSelects();
  }

  private initForm() {
    this.formData.items = [this.getDefaultItemValues()];
    this.setDefaultSelected();
  }

  private getDefaultItemValues(): InvoiceFormItem {
    return {
      quantity: '1',
      quantityUnit: this.DEFAULT_UNIT,
      text: '',
      vat: this.DEFAULT_VAT,
      price: '0'
    };
  }

  public addItem(): void {
    this.formData.items.push(this.getDefaultItemValues());
  }

  public async removeItem(index: number): Promise<void> {
    if (this.formData.items.length > 1) {
      this.formData.items.splice(index, 1);
    } else {
      const toast = await this.toastController.create({
        message: 'Nelze smazat jedinou položku.',
        duration: 2000
      });
      toast.present();
    }
  }

  public createInvoice(): void {
    this.invoiceService.createInvoice(this.formData).then(data => this.routerService.goToPageWithId(PageUrl.INVOICE, data.id));
  }

  public updateInvoice(): void {
    this.invoiceService.updateInvoice(this.formData, this.invoiceId).then(data => this.routerService.goToPageWithId(PageUrl.INVOICE, data.id));
  }

  private prepareSelects(): void {
    this.storageService.getPaymentMethods().forEach(pm => this.paymentMethodsForSelect.push({ id: pm.paymentMethodId.toString(), name: pm.name }));
    this.storageService.getNumberSeries().forEach(ns => {
      if (ns.type === 1) {
        this.numberSeriesForSelect.push({ id: ns.numberSerieId.toString(), name: ns.name })
      }
    });
    this.contactService.getContactList().then(contacts => contacts.forEach(e => this.customersForSelect.push({ id: e.contactId.toString(), name: e.name })));
    if (this.storageService.isVatPayer() === false) {
      this.formData.items.forEach(item => item.vat = "0");
    }
  }

  private setDefaultSelected(): void {
    this.formData.numberSeriesId = this.storageService.getNumberSeries()[0].numberSerieId.toString();
    this.formData.paymentMethodId = this.storageService.getPaymentMethods()[0].paymentMethodId.toString();
    this.formData.dateCreated = this.dateService.getTodayDate();
    this.formData.daysDue = this.DEFAULT_DUE_DAYS;
    this.formData.currency = this.DEFAULT_CURRENCY;
  }

  private loadInvoice(invoiceId: number) {
    this.invoiceService.getInvoice(invoiceId).then(invoice => {
      this.formData = this.invoiceService.convertInvoiceToInvoiceForm(invoice);
      setTimeout(() => this.changeRef.detectChanges(), 100);
    });
  }

  public async customerChanged(event): Promise<void> {
    let contactId = event.target.value;
    let contact: Contact = await this.contactService.getContact(contactId);
    this.formData = this.invoiceService.addCustomerToInvoice(this.formData, contact);
  }
}
