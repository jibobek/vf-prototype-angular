import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { PageUrl } from 'src/app/enum/PageUrl';
import { InvoiceService } from 'src/app/services/invoice.service';
import { RouterService } from 'src/app/services/router.service';
import { Invoice } from 'src/app/types/Invoice';
import { InvoiceFlags } from 'src/app/types/InvoiceFlags';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {

  public invoices: Array<Invoice> = [];
  public isLoading: boolean = false;
  public searchQuery: string = "";

  constructor(
    public invoiceService: InvoiceService,
    private routerService: RouterService,
    private platform: Platform,
  ) {
    this.platform.backButton.subscribeWithPriority(10, () => this.routerService.goToPage(PageUrl.HOME));
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.loadInvoices();
  }

  public loadInvoices() {
    this.isLoading = true;
    this.invoiceService.getInvoiceList(this.searchQuery).then(invoices => {
      this.invoices = invoices
      this.isLoading = false;
    });
  }

  public loadNext(event) {
    this.invoiceService.getInvoiceListNext(this.searchQuery).then(invoices => {
      this.invoices = this.invoices.concat(invoices);
      event.target.complete();
    });
  }

  public openInvoice(invoiceId: number) {
    this.routerService.goToPageWithId(PageUrl.INVOICE, invoiceId);
  }

  public createInvoice() {
    this.routerService.goToPageWithId(PageUrl.INVOICE_CREATE, 0);
  }

  public searchChanged(event): void {
    this.loadInvoices();
  }
}
