import { Component, OnInit } from '@angular/core';
import { PageUrl } from 'src/app/enum/PageUrl';
import { LoginService } from 'src/app/services/login.service';
import { RouterService } from 'src/app/services/router.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public formData = {
    email: '',
    password: ''
  };

  public isRunning: boolean = false;

  constructor(
    private loginService: LoginService,
    private routerService: RouterService,
  ) { }

  ngOnInit() {
  }

  public login() {
    this.isRunning = true;
    this.loginService.loginWithCredentials(this.formData.email, this.formData.password).then(() => {
      this.formData.password = '';
      this.isRunning = false;
      this.routerService.goToPage(PageUrl.HOME)
    }).catch(() => {
      this.isRunning = false;
    })
  }

}
