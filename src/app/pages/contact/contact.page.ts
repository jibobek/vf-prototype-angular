import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/types/Contact';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { RouterService } from 'src/app/services/router.service';
import { PageUrl } from 'src/app/enum/PageUrl';
import { ToastService } from 'src/app/services/toast.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  public contact: Contact;
  public contactId: number = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private contactService: ContactService,
    private routerService: RouterService,
    private toastService: ToastService,
    private platform: Platform,
  ) {
    this.platform.backButton.subscribeWithPriority(10, () => this.routerService.goToPage(PageUrl.DIRECTORY));
  }

  ngOnInit() {
    this.contactId = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewDidEnter() {
    if (this.contactId > 0) {
      this.loadContact();
    } else {
      this.loadEmptyData();
    }
  }

  private loadContact(): Promise<Contact | void> {
    return this.contactService.getContact(this.contactId).then(contact => this.contact = contact);
  }

  public updateContact(): void {
    this.contactService.updateContact(this.contact).then(() => {
      this.toastService.show("Kontakt upraven.");
      this.routerService.goToPage(PageUrl.DIRECTORY);
    });
  }

  public saveNewContact(): void {
    this.contactService.createNewContact(this.contact).then(() => {
      this.toastService.show("Kontakt uložen.");
      this.routerService.goToPage(PageUrl.DIRECTORY);
    });
  }

  public deleteContact(): void {
    this.contactService.deleteContact(this.contact).then(() => {
      this.routerService.goToPage(PageUrl.DIRECTORY);
      this.toastService.show("Kontakt smazán.");
    });
  }

  private loadEmptyData(): void {
    this.contact = this.contactService.getEmptyContact();
  }
}
