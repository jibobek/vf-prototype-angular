export interface InvoiceItem {
    quantity: number,
    unit: string,
    text: string,
    unitPrice: number,
    vatRate: number,
}
