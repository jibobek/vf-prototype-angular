export interface InvoiceForm {
    customerId: string,
    number: string,
    paymentMethodId: string,
    numberSeriesId: string,
    daysDue: string,
    dateCreated: string,
    currency: string,
    vs: string,
    ks: string,
    ss: string,
    items: Array<InvoiceFormItem>,
    customerIco: string,
    customerDic: string,
    customerName: string,
    customerStreet: string,
    customerCity: string,
    customerZip: string,
    customerCountryCode: string,
}

export interface InvoiceFormItem {
    quantity: string,
    quantityUnit: string,
    text: string,
    vat: string,
    price: string
}
