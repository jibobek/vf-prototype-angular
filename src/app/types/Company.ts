export interface Company {
    companyId: number,
    name: string,
    apiKey: string,
    flags: number,
}
