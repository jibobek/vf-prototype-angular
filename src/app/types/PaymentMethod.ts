export interface PaymentMethod {
    paymentMethodId: number,
    flags: number,
    type: number,
    name: string,
}
