export interface InvoiceVat {
    vat_rate: number,
    base: number,
    vat: number,
    total: number,
}
