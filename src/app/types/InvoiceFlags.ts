export interface InvoiceFlags {
    withDph: boolean,
    paid: boolean,
    sendByEmail: boolean,
    storno: boolean,
    reminderSent: boolean,
    overpayment: boolean,
    unpaidBalance: boolean,
    downloadedByAccountant: boolean,
    archived: boolean
}
