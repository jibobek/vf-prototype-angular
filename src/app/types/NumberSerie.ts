export interface NumberSerie {
    numberSerieId: number,
    type: number,
    name: string,
    pattern: string,
}
