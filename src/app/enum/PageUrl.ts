export enum PageUrl {
    HOME = '/home',
    LOGIN = '/login',
    INVOICES = '/invoices',
    INVOICE = '/invoice',
    INVOICE_CREATE = '/create',
    CONTACT = '/contact',
    DIRECTORY = '/directory',
}
