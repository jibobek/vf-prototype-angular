import { Injectable } from '@angular/core';
import { Contact } from '../types/Contact';
import { Invoice } from '../types/Invoice';
import { InvoiceFlags } from '../types/InvoiceFlags';
import { InvoiceForm, InvoiceFormItem } from '../types/InvoiceForm';
import { InvoiceItem } from '../types/InvoiceItem';
import { InvoiceVat } from '../types/InvoiceVat';
import { PaymentMethod } from '../types/PaymentMethod';
import { AlertService } from './alert.service';
import { EndpointService } from './endpoint.service';
import { ListFilterService } from './list-filter.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(
    private listFilterService: ListFilterService,
    private endPointService: EndpointService,
    private storageService: StorageService,
    private alertService: AlertService,
  ) { }


  public convertApiInvoice(invoice: any): Invoice {
    let invoiceItems: Array<InvoiceItem> = [];
    if (invoice.items) {
      for (let i = 0; i < invoice.items.length; i++) {
        const item = invoice.items[i];
        invoiceItems.push({
          quantity: +item.quantity,
          unit: item.unit,
          text: item.text,
          unitPrice: +item.unit_price,
          vatRate: +item.vat_rate,
        });
      }
    }
    return {
      invoiceId: +invoice.id,
      customerId: +invoice.id_customer,
      numberSerieId: +invoice.id_number_series,
      paymentMethodId: +invoice.id_payment_method,
      number: invoice.number,
      flags: +invoice.flags,
      dateCreated: invoice.date_created,
      dateDue: invoice.date_due,
      datePaid: invoice.date_paid,
      daysDue: +invoice.days_due,
      supplierIco: invoice.supplier_IC,
      supplierDic: invoice.supplier_DIC,
      supplierName: invoice.supplier_name,
      supplierStreet: invoice.supplier_street,
      supplierCity: invoice.supplier_city,
      supplierZip: invoice.supplier_zip,
      supplierCountryCode: invoice.supplier_country_code,
      customerIco: invoice.customer_IC,
      customerDic: invoice.customer_DIC,
      customerName: invoice.customer_name,
      customerStreet: invoice.customer_street,
      customerCity: invoice.customer_city,
      customerZip: invoice.customer_zip,
      customerCountryCode: invoice.customer_country_code,
      mailTo: invoice.mail_to,
      vs: +invoice.VS,
      ks: +invoice.KS,
      ss: +invoice.SS,
      currency: invoice.currency,
      total: +invoice.total,
      eetStatus: +invoice.eet_status,
      publicUrl: invoice.url_public_webpage,
      totalWithoutVat: +invoice.total_without_vat,
      items: invoiceItems,
      vats: invoice.vats as Array<InvoiceVat>,
    }
  }

  public convertInvoiceFormToApi(formData: InvoiceForm, invoiceId: number = 0) {
    let items: Array<any> = [];
    formData.items.forEach(e => {
      items.push({
        quantity: e.quantity,
        unit: e.quantityUnit,
        text: e.text,
        vat_rate: e.vat,
        unit_price: e.price
      });
    });
    let data: any = {
      id_customer: formData.customerId,
      number: formData.number,
      id_payment_method: formData.paymentMethodId,
      id_number_series: formData.numberSeriesId,
      days_due: formData.daysDue,
      date_create: formData.dateCreated,
      currency: formData.currency,
      VS: formData.vs,
      KS: formData.ks,
      SS: formData.ss,
      items: items,
      customer_IC: formData.customerIco,
      customer_DIC: formData.customerDic,
      customer_name: formData.customerName,
      customer_street: formData.customerStreet,
      customer_city: formData.customerCity,
      customer_zip: formData.customerZip,
      customer_country_code: formData.customerCountryCode,
    }
    if (invoiceId > 0) {
      data.id = invoiceId;
    }
    return data;
  }

  public convertInvoiceToInvoiceForm(invoice: Invoice): InvoiceForm {
    let items: Array<InvoiceFormItem> = [];
    invoice.items.forEach(e =>
      items.push({
        quantity: e.quantity.toString(),
        quantityUnit: e.unit,
        text: e.text,
        vat: e.vatRate.toString(),
        price: e.unitPrice.toString()
      })
    );
    return {
      customerId: invoice.customerId.toString(),
      number: invoice.number,
      paymentMethodId: invoice.paymentMethodId.toString(),
      numberSeriesId: invoice.numberSerieId.toString(),
      daysDue: invoice.daysDue.toString(),
      dateCreated: invoice.dateCreated,
      currency: invoice.currency,
      vs: ((invoice.vs > 0) ? invoice.vs.toString() : ''),
      ks: ((invoice.ks > 0) ? invoice.ks.toString() : ''),
      ss: ((invoice.ss > 0) ? invoice.ss.toString() : ''),
      items: items,
      customerIco: invoice.customerIco,
      customerDic: invoice.customerDic,
      customerName: invoice.supplierName,
      customerStreet: invoice.customerStreet,
      customerCity: invoice.customerCity,
      customerZip: invoice.customerZip,
      customerCountryCode: invoice.customerCountryCode,
    }
  }

  public convertApiInvoiceList(invoices: Array<any>): Array<Invoice> {
    let list: Array<Invoice> = [];
    for (let i = 0; i < invoices.length; i++) {
      list.push(this.convertApiInvoice(invoices[i]));
    }
    return list;
  }

  public getInvoiceList(searchQuery: string): Promise<Array<Invoice>> {
    return new Promise(resolve => {
      this.listFilterService.invoiceListResetPage();
      this.listFilterService.updateInvoiceListSearchQuery(searchQuery);
      this.endPointService.getInvoices(this.listFilterService.getInvoiceListFilter()).then(items => resolve(this.convertApiInvoiceList(items)));
    });
  }

  public getInvoiceListNext(searchQuery: string): Promise<Array<Invoice>> {
    return new Promise(resolve => {
      this.listFilterService.invoiceListNextPage();
      this.listFilterService.updateInvoiceListSearchQuery(searchQuery);
      this.endPointService.getInvoices(this.listFilterService.getInvoiceListFilter()).then(items => resolve(this.convertApiInvoiceList(items)));
    });
  }

  public getInvoice(invoiceId: number): Promise<Invoice> {
    return new Promise(resolve => this.endPointService.getInvoice(invoiceId).then(invoiceApi => resolve(this.convertApiInvoice(invoiceApi))));
  }


  public translateInvoiceFlags(flags: number): InvoiceFlags {
    return {
      withDph: (flags & 1) > 0,
      paid: (flags & 2) > 0,
      sendByEmail: (flags & 4) > 0,
      storno: (flags & 8) > 0,
      reminderSent: (flags & 16) > 0,
      overpayment: (flags & 32) > 0,
      unpaidBalance: (flags & 64) > 0,
      downloadedByAccountant: (flags & 256) > 0,
      archived: (flags & 4096) > 0
    };
  }

  getInvoiceReadableFlags(bitwise: number): Array<{ name: string, color: string }> {
    let flags: InvoiceFlags = this.translateInvoiceFlags(bitwise);
    let badges = [];
    Object.keys(flags).forEach(flag => {
      let text = "";
      let color = "";
      if (flags[flag]) {
        switch (flag) {
          case "withDph": {
            text = "Obsahuje DPH"; color = "light"; break;
          }
          case "paid": {
            text = "Zaplaceno"; color = "success"; break;
          }
          case "sendByEmail": {
            text = "Odesláno"; color = "primary"; break;
          }
          case "storno": {
            text = "Stornováno"; color = "danger"; break;
          }
          case "reminderSent": {
            text = "Připomínka"; color = "light"; break;
          }
          case "overpayment": {
            text = "Přeplaceno"; color = "warning"; break;
          }
          case "unpaidBalance": {
            text = "Nedoplaceno"; color = "warning"; break;
          }
          case "downloadedByAccountant": {
            text = "Účetní"; color = "medium"; break;
          }
          case "archived": {
            text = "Archivováno"; color = "dark"; break;
          }
        }
        badges.push({ name: text, color: color });
      }
    });
    return badges;
  }

  public translateCurrencyToSymbol(price: number, currencySymbol: string): string {
    return new Intl.NumberFormat('cs-CZ', { style: 'currency', currency: currencySymbol }).format(price);
  }

  public getPaymentMethod(paymentMethodId: number): PaymentMethod {
    return this.storageService.getPaymentMethods().find(paymentMethod => paymentMethod.paymentMethodId === paymentMethodId);
  }

  public openPublicInvoicePage(invoice: Invoice): void {
    window.open(invoice.publicUrl, '_system', 'location=yes');
  }

  public archiveInvoice(invoice: Invoice, archive: boolean): Promise<any> {
    return this.endPointService.archiveInvoice(invoice, archive);
  }

  public stornoInvoice(invoice: Invoice, storno: boolean): Promise<any> {
    return this.endPointService.stornoInvoice(invoice, storno);
  }

  public sendToEet(invoice: Invoice): Promise<any> {
    return this.endPointService.sendInvoiceToEet(invoice);
  }

  public delete(invoice: Invoice): Promise<any> {
    return this.endPointService.deleteInvoice(invoice);
  }

  public payInvoice(invoice: Invoice): Promise<void> {
    return new Promise(resolve =>
      this.alertService.showDate('Uhrazení faktury', 'Nastavte datum uhrazení dokladu.', handler =>
        this.endPointService.payInvoice(invoice, handler.datePaid).then(() =>
          resolve()), 'Uhradit', 'Zrušit', 'datePaid'));
  }

  public createInvoice(invoiceForm: InvoiceForm): Promise<{ id: number, data: any }> {
    let dataForApi = this.convertInvoiceFormToApi(invoiceForm);
    return new Promise(resolve => {
      this.endPointService.createInvoice(dataForApi).then(response => {
        if (response.status == 'error') {
          this.alertService.show('Chyba!', response.message[Object.keys(response.message)[0]][0]);
        } else {
          resolve({ id: +response.id, data: response });
        }
      });
    });
  }

  public updateInvoice(invoiceForm: InvoiceForm, invoiceId: number): Promise<{ id: number, data: any }> {
    let dataForApi = this.convertInvoiceFormToApi(invoiceForm, invoiceId);
    return new Promise(resolve => {
      this.endPointService.updateInvoice(dataForApi).then(response => {
        if (response.status == 'error') {
          this.alertService.show('Chyba!', response.message[Object.keys(response.message)[0]][0]);
        } else {
          resolve({ id: +response.id, data: response });
        }
      });
    });
  }

  public addCustomerToInvoice(invoice: InvoiceForm, customer: Contact): InvoiceForm {
    invoice.customerIco = customer.ico;
    invoice.customerDic = customer.dic;
    invoice.customerName = customer.company;
    invoice.customerStreet = customer.street;
    invoice.customerCity = customer.city;
    invoice.customerZip = customer.zip;
    invoice.customerCountryCode = customer.countryCode;
    return invoice;
  }
}
