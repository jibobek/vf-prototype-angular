import { Injectable } from '@angular/core';
import { Contact } from '../types/Contact';
import { AlertService } from './alert.service';
import { EndpointService } from './endpoint.service';
import { ListFilterService } from './list-filter.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(
    private endPointService: EndpointService,
    private listFilterService: ListFilterService,
    private alertService: AlertService,
  ) { }

  public convertApiToContact(contact: any): Contact {
    return {
      contactId: +contact.id,
      ico: contact.IC,
      dic: contact.DIC,
      name: contact.name,
      note: contact.note,
      company: contact.company,
      firstName: contact.firstname,
      lastName: contact.lastname,
      street: contact.street,
      city: contact.city,
      zip: contact.zip,
      countryCode: contact.country_code,
      web: contact.web,
      tel: contact.tel,
      mailTo: contact.mail_to,
      deliveryCity: contact.delivery_city,
      deliveryCompany: contact.delivery_company,
      deliveryCountryCode: contact.delivery_country_code,
      deliveryFirstName: contact.delivery_firstname,
      deliveryLastName: contact.delivery_lastname,
      deliveryStreet: contact.delivery_street,
      deliveryTel: contact.delivery_tel,
      deliveryZip: contact.delivery_zip,
      textBeforeItems: contact.text_before_items,
      textInvoiceFooter: contact.text_invoice_footer,
      textUnderSubscriber: contact.text_under_subscriber,
    }
  }

  public convertContactToApi(contact: Contact): any {
    return {
      id: contact.contactId,
      IC: contact.ico,
      DIC: contact.dic,
      name: contact.name,
      note: contact.note,
      company: contact.company,
      firstname: contact.firstName,
      lastname: contact.lastName,
      street: contact.street,
      city: contact.city,
      zip: contact.zip,
      country_code: contact.countryCode,
      web: contact.web,
      tel: contact.tel,
      mail_to: contact.mailTo,
      delivery_city: contact.deliveryCity,
      delivery_company: contact.deliveryCompany,
      delivery_country_code: contact.deliveryCountryCode,
      delivery_firstname: contact.deliveryFirstName,
      delivery_lastname: contact.deliveryLastName,
      delivery_street: contact.deliveryStreet,
      delivery_tel: contact.deliveryTel,
      delivery_zip: contact.deliveryZip,
      text_before_items: contact.textBeforeItems,
      text_invoice_footer: contact.textInvoiceFooter,
      text_under_subscriber: contact.textUnderSubscriber,
    }
  }

  public convertApiContactList(contacts: Array<any>): Array<Contact> {
    let list: Array<Contact> = [];
    for (let i = 0; i < contacts.length; i++) {
      list.push(this.convertApiToContact(contacts[i]));
    }
    return list;
  }

  public getContactList(): Promise<Array<Contact>> {
    return new Promise(resolve => {
      this.listFilterService.contactListResetPage();
      this.endPointService.getContacts(this.listFilterService.getContactListFIlter()).then(items => resolve(this.convertApiContactList(items)));
    });
  }

  public getContactListNext(): Promise<Array<Contact>> {
    return new Promise(resolve => {
      this.listFilterService.contactListNextPage();
      this.endPointService.getContacts(this.listFilterService.getContactListFIlter()).then(items => resolve(this.convertApiContactList(items)));
    });
  }

  public getContact(contactId: number): Promise<Contact> {
    return new Promise(resolve => this.endPointService.getContact(contactId).then(item => resolve(this.convertApiToContact(item))));
  }

  public deleteContact(contact: Contact): Promise<void> {
    return this.endPointService.deleteContact(contact);
  }

  public updateContact(contact: Contact): Promise<void> {
    if (this.validateSaveContact(contact) === false) {
      return new Promise(resolve => { });
    }
    let apiContact = this.convertContactToApi(contact);
    return new Promise(resolve => {
      this.endPointService.updateContact(apiContact).then(response => {
        if (response.status !== 'error') {
          resolve();
        } else {
          if (response.message) {
            let keys = Object.keys(response.message);
            let error = (keys.length > 0) ? response.message[keys[0]][0] : '';
            this.alertService.show('Chyba!', error);
          }
        }
      });
    });
  }

  public createNewContact(contact: Contact): Promise<void> {
    if (this.validateSaveContact(contact) === false) {
      return new Promise(resolve => { });
    }
    let apiContact = this.convertContactToApi(contact);
    return new Promise(resolve => {
      this.endPointService.createContact(apiContact).then(response => {
        if (response.status !== 'error') {
          resolve();
        } else {
          if (response.message) {
            let keys = Object.keys(response.message);
            let error = (keys.length > 0) ? response.message[keys[0]][0] : '';
            this.alertService.show('Chyba!', error);
          }
        }
      });
    });
  }

  public getEmptyContact(): Contact {
    return {
      contactId: 0,
      ico: '',
      dic: '',
      name: '',
      note: '',
      company: '',
      firstName: '',
      lastName: '',
      street: '',
      city: '',
      zip: '',
      countryCode: '',
      web: '',
      tel: '',
      mailTo: '',
      deliveryCity: '',
      deliveryCompany: '',
      deliveryCountryCode: '',
      deliveryFirstName: '',
      deliveryLastName: '',
      deliveryStreet: '',
      deliveryTel: '',
      deliveryZip: '',
      textBeforeItems: '',
      textInvoiceFooter: '',
      textUnderSubscriber: '',
    }
  }

  private validateSaveContact(contact: Contact): boolean {
    if (contact.name === '') {
      this.alertService.show('Chyba!', "Není vyplněn název.");
      return false;
    }
    return true;
  }
}
