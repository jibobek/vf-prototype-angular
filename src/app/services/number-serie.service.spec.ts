import { TestBed } from '@angular/core/testing';

import { NumberSerieService } from './number-serie.service';

describe('NumberSerieService', () => {
  let service: NumberSerieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NumberSerieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
