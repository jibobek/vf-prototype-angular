import { TestBed } from '@angular/core/testing';

import { ListFilterService } from './list-filter.service';

describe('ListFilterService', () => {
  let service: ListFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
