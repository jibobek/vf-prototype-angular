import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListFilterService {

  private readonly ITEMS_PER_PAGE = 10;

  private contactListFilter = {
    q: '',
    rows_limit: this.ITEMS_PER_PAGE,
    rows_offset: 0,
    sort: 'name~asc',
  };

  private invoiceListfilter = {
    rows_limit: this.ITEMS_PER_PAGE,
    rows_offset: 0,
    date_created_from: '2022-01-01',
    date_created_to: '2022-12-31',
    sort: 'number~desc',
    filter: '',
    q: '',
  };

  constructor() { }

  public getContactListFIlter(): object {
    return this.contactListFilter;
  }

  public contactListNextPage(): void {
    this.contactListFilter.rows_offset += this.ITEMS_PER_PAGE;
  }

  public contactListResetPage(): void {
    this.contactListFilter.rows_offset = 0;
  }

  public getInvoiceListFilter(): object {
    return this.invoiceListfilter;
  }

  public invoiceListNextPage(): void {
    this.invoiceListfilter.rows_offset += this.ITEMS_PER_PAGE;
  }

  public invoiceListResetPage(): void {
    this.invoiceListfilter.rows_offset = 0;
  }

  public updateInvoiceListSearchQuery(searchQuery: string): void {
    this.invoiceListfilter.q = searchQuery;
  }
}
